#import "ProgramList.h"

@implementation ProgramList

- (instancetype)initWithJSON:(NSDictionary *)json {
    if ([super init]) {
        NSArray *programs = json[@"listOfPrograms"];
        if (programs && programs.count > 0) {
            NSMutableArray *programArray = [NSMutableArray array];
            for (NSDictionary *programDictionary in programs) {
                Program *program = [[Program alloc] initWithJSON:programDictionary];
                [programArray addObject:program];
            }
            self.programList = [programArray copy];
        }
    }
    return self;
}

@end
