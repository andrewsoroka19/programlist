#import "Program.h"

@implementation Program

- (instancetype)initWithJSON:(NSDictionary *)json {
    if ([super init]) {
        self.programTitle = json[@"title"];
        self.programDescription = json[@"description"];
        
        NSString *URLString = json[@"image"];
        if (URLString) {
            self.imageURL = [[NSURL alloc] initWithString:URLString];
        }
    }
    return self;
}

@end
