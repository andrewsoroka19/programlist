#import <Foundation/Foundation.h>

@interface Program : NSObject

@property (copy) NSString *programTitle;
@property (copy) NSString *programDescription;
@property (strong, nonatomic) NSURL *imageURL;

- (instancetype)initWithJSON:(NSDictionary *)json;

@end
