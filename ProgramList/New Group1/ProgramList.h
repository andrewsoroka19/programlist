#import <Foundation/Foundation.h>
#import "Program.h"

@interface ProgramList : NSObject

@property (strong, nonatomic) NSArray <Program *> *programList;

- (instancetype)initWithJSON:(NSDictionary *)json;

@end
