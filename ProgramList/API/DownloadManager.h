#import <Foundation/Foundation.h>
#import "ProgramList.h"

@interface DownloadManager : NSObject

+ (void)fetchProgramsWithCompletionHandler:(void(^)(ProgramList *))handler;

@end
