#import <AFNetworking/AFNetworking.h>
#import "DownloadManager.h"

static NSString *const RequestURLString = @"https://test-rap.000webhostapp.com/FirstScene.json";

@implementation DownloadManager

+ (void)fetchProgramsWithCompletionHandler:(void(^)(ProgramList *))handler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:RequestURLString
      parameters:nil
        progress:^(NSProgress * _Nonnull downloadProgress) {}
         success:^(NSURLSessionDataTask *task, id responseObject) {
             
             if ([responseObject isKindOfClass:NSDictionary.class]) {
                 ProgramList *list = [[ProgramList alloc]initWithJSON:responseObject];
                 handler(list);
             }
             else {
                 handler(nil);
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             handler(nil);
         }];
}

@end
