import UIKit

final class ProgramListViewController: UITableViewController {
    @IBOutlet private weak var activity: UIActivityIndicatorView!
    
    var programs = [Program]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        downloadPrograms()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProgramTableViewCell", for: indexPath) as? ProgramTableViewCell else { return ProgramTableViewCell() }
        cell.model = programs[indexPath.row]
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let programDetailsController = storyboard?.instantiateViewController(withIdentifier: ProgramDetailsViewController.identifier) as? ProgramDetailsViewController else { return }
        programDetailsController.model = programs[indexPath.row]
        navigationController?.pushViewController(programDetailsController, animated: true)
    }
    
    private func downloadPrograms() {
        tableView.showLoadingIndicator()
        DownloadManager.fetchPrograms { [weak self] programList in
            self?.tableView.hideLoadingIndicator()
            if let programList = programList?.programList {
                self?.programs = programList
                self?.tableView.reloadData()
            } else {
                self?.showErrorAlert()
            }
        }
    }
    private func showErrorAlert() {
        let alertController = UIAlertController(title: "Error", message: "Some problem with the internet!", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
