import UIKit

final class ProgramDetailsViewController: UIViewController {
    static let identifier = "ProgramDetailsViewController"
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var descriptionTextView: UITextView!
    @IBOutlet private weak var activity: UIActivityIndicatorView!
    @IBOutlet private weak var titleLabel: UILabel?
    
    var model: Program?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = model?.programTitle
        titleLabel?.text = model?.programTitle
        descriptionTextView.text = model?.programDescription
        
        activity.startAnimating()
        imageView.sd_setImage(with: model?.imageURL) { [weak self] (_, _, _, _) in
            self?.activity.stopAnimating()
        }
    }
}
