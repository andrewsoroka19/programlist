import UIKit

extension UITableView {
    
    func showLoadingIndicator() {
        if isLoading() { return }
        tableFooterView = TableActivityIndicatorView()
    }
    
    func hideLoadingIndicator() {
        tableFooterView = nil
    }
    
    func isLoading() -> Bool {
        return tableFooterView is TableActivityIndicatorView
    }
}

class TableActivityIndicatorView: UIView {
    
    lazy var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    init() {
        super.init(coder: NSCoder.empty())!
        frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        appendActivityIndicator()
        activityIndicator.startAnimating()
    }
    
    convenience required init?(coder aDecoder: NSCoder) {
        self.init()
    }
    
    private func appendActivityIndicator() {
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalConstrains = NSLayoutConstraint(item: activityIndicator,
                                                      attribute: NSLayoutAttribute.centerX,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self,
                                                      attribute: NSLayoutAttribute.centerX,
                                                      multiplier: 1,
                                                      constant: 0)
        
        let verticalConstrains = NSLayoutConstraint(item: activityIndicator,
                                                    attribute: NSLayoutAttribute.centerY,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self,
                                                    attribute: NSLayoutAttribute.centerY,
                                                    multiplier: 1,
                                                    constant: 0)
        
        addConstraints([horizontalConstrains, verticalConstrains])
    }
}

extension NSCoder {
    class func empty() -> NSCoder {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.finishEncoding()
        return NSKeyedUnarchiver(forReadingWith: data as Data)
    }
}
