import UIKit

final class ProgramTableViewCell: UITableViewCell {
    @IBOutlet private weak var programImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var activity: UIActivityIndicatorView!
    
    var model: Program? {
        didSet {
            titleLabel.text = model?.programTitle
            descriptionLabel.text = model?.programDescription
            
            activity.startAnimating()
            programImageView.sd_setImage(with: model?.imageURL) { [weak self] (_, _, _, _) in
                self?.activity.stopAnimating()
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        programImageView.image = nil
        titleLabel.text = nil
        descriptionLabel.text = nil
    }
}
